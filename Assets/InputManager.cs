﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class InputManager : MonoBehaviour
{

    public static InputManager S;

    public Action OnEscPressed;

    private void Awake()
    {
        S = this;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (OnEscPressed != null) OnEscPressed();
        }

    }
}
