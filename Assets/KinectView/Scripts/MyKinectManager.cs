﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System.Diagnostics;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class MyKinectManager : MonoBehaviour
{
    public static MyKinectManager S;
                                   
    public GameObject BodySourceManager;
    
        [Header("IsBodytrackedByKinect:")]
    public bool BodyTracked = false;

    public static int frameCounter = 0;

    [Header("KinectScale:")]
    public Vector3 OffsetKinectScale;

    [Header("Joint Positions:")]
    public Vector3 spinePos;
    public Vector3 rightHandPos;
    public Vector3 leftHandPos;

    public Action<Vector3> OnSpinePos;
    public Action<Vector3> OnRightHandPos;
    public Action<Vector3> OnLeftHandPos;

    public static Vector3 ballsoo = new Vector3(5, 2, 14);
    public static int IsTracked = 0;
    public static int[] BallOrder = new int[100];
    public static int nballs = 4; // number of diffrent ball's locations
    public static int tball = 9; // number of each ball's repeats
    public static float isi = 3F; // balls frequency (in sec)
    private static float minCameraDistance = 1F; // (in meters)
    private static float minWristRadius = 0.25F; // (in meters)
                                   
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;

    private string activeJoint; // the joint that hit the ball

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
        { Kinect.JointType.Head, Kinect.JointType.Head },
    };

    Stopwatch stopWatch = new Stopwatch(); //used in order to track time and fix frame rate
    public bool isShowKinektBody;

    private void Awake()
    {
        S = this;
    }

    private void Start()
    {
        InputManager.S.OnEscPressed += OnEscPressed;
    }

    private void OnEscPressed()
    {
        UnityEngine.Debug.Log("OnEscPressed BodySourceManager");
    
        BodySourceManager.SetActive(false);
    }

    void FixedUpdate () 
    {
        frameCounter++;
        
        if (BodySourceManager == null)
        {
            return;
        }
        
        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }
        
        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            BodyTracked = false;
            return;
        }
        
        List<ulong> trackedIds = new List<ulong>();
        /*foreach(var body in data)
        {
            if (body == null)
            {
                continue;
            }
                
            if(body.IsTracked)
            {
                trackedIds.Add(body.TrackingId);
            }
        }*/

        // Track only the nearest body
        ulong trackedId = GetNearestBodyTrackingId(data);
        if (trackedId > 0)
            trackedIds.Add(trackedId);

        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach(var body in data)
        {
            if (body == null) continue;
            
            if(body.IsTracked && body.TrackingId == trackedId)
            {
                if(!_Bodies.ContainsKey(body.TrackingId))
                {
                    UnityEngine.Debug.Log("WTF");
                    _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                }

                BodyTracked = true;
                RefreshBodyObject(body, _Bodies[body.TrackingId]);
          //      EnableBallons(body);

                Kinect.JointType spine = Kinect.JointType.SpineShoulder;
                Kinect.Joint spinesource = body.Joints[spine];
                ballsoo = GetVector3FromJoint(spinesource);
            }
        }
    }
    
    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            if (isShowKinektBody)
            {
                

                LineRenderer lr = jointObj.AddComponent<LineRenderer>();
                lr.SetVertexCount(2);
                lr.SetWidth(0.05f, 0.05f);
                jointObj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                
            }
            else
            {
                Renderer rend = jointObj.GetComponent<Renderer>();
                rend.enabled = false;
            }
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }
        
        return body;
    }

    private ulong GetNearestBodyTrackingId(Kinect.Body[] bodies)
    {
        float distance = 1000;
        ulong nearestBodyTrackingId = 0;

        foreach (var body in bodies)
        {
            if (body == null)
            {
                continue;
            }
            if (body.IsTracked)
            {
                float distanceNew = body.Joints[Kinect.JointType.Head].Position.Z;
                if (distanceNew != 0 && distanceNew <= distance)
                {
                    distance = distanceNew;
                    nearestBodyTrackingId = body.TrackingId;
                }
            }
        }

        return nearestBodyTrackingId;
    }
    
    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {    
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];

            Kinect.Joint? targetJoint = null;

            if (_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            Transform jointObj = bodyObject.transform.Find(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            //   if (_BoneMap.ContainsKey(targetJoint))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }

            if (isShowKinektBody)
                if (targetJoint.HasValue)
                    if (targetJoint.Value != null)
                    {
                        LineRenderer lr = jointObj.GetComponent<LineRenderer>();
                        lr.SetPosition(0, jointObj.localPosition);
                        lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                        lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
                    }
        }

        StringBuilder str = new StringBuilder();

        foreach (var targetJoint in body.Joints)
        {
            str.Append((targetJoint.Value.JointType.ToString() + ": (" + targetJoint.Value.Position.X.ToString("F5") + "," + targetJoint.Value.Position.Y.ToString("F5") + "," + targetJoint.Value.Position.Z.ToString("F5") + "), "));

            spinePos = leftHandPos = rightHandPos = new Vector3(-1000, -1000, -1000);

            switch (targetJoint.Value.JointType)
            {
                case Kinect.JointType.SpineBase:
                    spinePos = new Vector3(targetJoint.Value.Position.X * OffsetKinectScale.x, targetJoint.Value.Position.Y * OffsetKinectScale.y, targetJoint.Value.Position.Z * OffsetKinectScale.z);
                    if (OnSpinePos != null) OnSpinePos(spinePos);
                    //}
                    break;
                case Kinect.JointType.HandTipLeft:
                    leftHandPos = new Vector3(targetJoint.Value.Position.X * OffsetKinectScale.x, targetJoint.Value.Position.Y * OffsetKinectScale.y, targetJoint.Value.Position.Z * OffsetKinectScale.z);
                    if (OnLeftHandPos != null) OnLeftHandPos(leftHandPos);

                    break;
                case Kinect.JointType.HandTipRight:
                    rightHandPos = new Vector3(targetJoint.Value.Position.X * OffsetKinectScale.x, targetJoint.Value.Position.Y * OffsetKinectScale.y, targetJoint.Value.Position.Z * OffsetKinectScale.z);
                    if (OnRightHandPos != null) OnRightHandPos(rightHandPos);

                    break;

                default:
                    break;
            }

              //    stringArray[a] = FileManager.S.str + lastMS.ToString() + "," + Time.deltaTime.ToString() + "," + myevent + "," + activeJoint + ",";
        }
        str.AppendLine();
        str.AppendLine("Frame: " + frameCounter + " , Time: " + /*lastMS.ToString()*/ Time.time.ToString() + " , FixedDeltaTime: " + Time.fixedDeltaTime.ToString());// + "," + /*myevent*/" " + "," + activeJoint + ",");
        if (FileManager.S.OnAddToMovementLog != null) FileManager.S.OnAddToMovementLog(str.ToString());

    }



    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
          
}
