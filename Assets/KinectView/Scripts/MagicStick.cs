﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MagicStick : MonoBehaviour {

    public bool isHeld = false;

    [SerializeField]
    SetZToTransform setZ;

    public Action<SetOnPosition> OnMagicStickTriggerEnter;

    [SerializeField]
    Vector3 offSetFromSpineForRightHand;

    [SerializeField]
    Vector3 offSetFromSpineForLeftHand;

    [SerializeField]
    float setZto = -1f;

    [SerializeField]
    ParticleSystem particle;

    Animator anim;

    List<Renderer> renderers = new List<Renderer>();

    private void Awake()
    {
        anim = GetComponent<Animator>();
        renderers = GetComponentsInChildren<Renderer>().ToList();
    }

    internal void SetPosition(Vector3 pos, JointToTrack joint)
    {
        Vector3 newPos;
        if (joint == JointToTrack.LeftHand)
        {
            newPos = pos + offSetFromSpineForLeftHand;
            setZ.TransformToFollow = MyGameManager.S.LeftHandCollider.transform;

        }
        else
        {
            newPos = pos + offSetFromSpineForRightHand;
            setZ.TransformToFollow = MyGameManager.S.RightHandCollider.transform;
        }

        if (setZto != -1) newPos.z = setZto;

        transform.position = newPos;
    }

    private void OnTriggerEnter(Collider other)
    {
       // Debug.Log("MagicStick OnTriggerEnter");
        SetOnPosition gameJointColl = other.GetComponent<SetOnPosition>();
        if (gameJointColl == null) gameJointColl = other.GetComponentInParent<SetOnPosition>();
        if (gameJointColl != null)
        {
           // Debug.Log("MagicStick Touched by " + gameJointColl.name);
            if (OnMagicStickTriggerEnter != null)
                OnMagicStickTriggerEnter(gameJointColl);
        }
    }

    [SerializeField]
    Vector3 offsetHeldOnHand;

    public void HoldMagicStick(Transform _parent)
    {
        Debug.Log("Magic Stick Hold Stick");
        transform.parent = _parent;
        transform.localPosition = offsetHeldOnHand;
        isHeld = true;

        particle.gameObject.SetActive(true);

        anim.SetBool("IsScale", false);
    }

    internal void ReleaseStick()
    {
        transform.parent = null;
        isHeld = false;

        particle.gameObject.SetActive(false);

        anim.SetBool("IsScale", true);

    }

    public void SetMagicStickVisible(bool isVisible)
    {
        renderers.ForEach((r) => r.enabled = isVisible);

    }
}
