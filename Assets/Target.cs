﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Target : MonoBehaviour {
 
    public JointToTrack joint;

    [SerializeField]
    List<Collider> colliders = new List<Collider>();
    [SerializeField]
    SetZToTransform setZ;

    private void Awake()
    {
        colliders = GetComponentsInChildren<Collider>().ToList();
        setZ = GetComponent<SetZToTransform>();
    
    }

    
    private void OnTriggerEnter(Collider other)
    {
        MagicStick stick= other.GetComponent<MagicStick>();
        if (stick != null)
        {
          //  Debug.Log("targetTouhced!");
            //      if (OnTargetReached != null) OnTargetReached();
            //   if (ActionsManager.OnTargetReached != null) ActionsManager.OnTargetReached(true);
            if (ActionsManager.OnTargetTouched != null) ActionsManager.OnTargetTouched(true);

        }
    }

    private void OnTriggerExit(Collider other)
    {
        MagicStick stick = other.GetComponent<MagicStick>();
        if (stick != null)
        {
            Debug.Log("targetTouhced!");
            //      if (OnTargetReached != null) OnTargetReached();
            if (ActionsManager.OnTargetTouched != null) ActionsManager.OnTargetTouched(false);

        }
    }

    public void SetTargetAt(Vector3 pos)
    {
        ActivateTarget(true);
        transform.position = pos;
    }

    public void ActivateTarget(bool isActivate)
    {
        
        colliders.ForEach((c) => c.enabled = isActivate);
        setZ.enabled = isActivate;
    }
                                 
}
