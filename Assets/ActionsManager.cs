﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class ActionsManager {

    public static Action<bool> OnTargetReached;


    public static Action<int> OnGetTarget;

    public static Action OnMagicStickHold;

    public static Action OnTargetTimeUp;

    public static Action OnTargetTimerStart;

    public static Action<bool> OnTargetTouched;

    public static Action OnGameOver;

}
