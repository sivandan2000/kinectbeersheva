﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum JointToTrack
{
    None,
    RightHand,
    LeftHand,
    Spine
}

public class MyGameManager : MonoBehaviour {

    public static MyGameManager S;

    public int NumberOfTargetsNeeded = 20;
    public int TargetReachedCounter = 0;

    [Header("Timer:")]
    public float AllowedTimeToTarget;
    //[SerializeField]
    float t = 0;
    //[SerializeField]
    private bool isTimer;

    public float NeededTimeToTouchTarget = 0.5f;

    [Header("Target:")]
    [SerializeField]
    public Target target;
    Vector3 newTargetPos;

    [Header("HandColliders:")]
    public Transform LeftHandCollider;
    public Transform RightHandCollider;

    [Header("Which Joint To Track:")]

    public JointToTrack jointToTrack;


    [Header("The Magic Stick:")]
    public MagicStick magicStick;

    [Header("The Position Of The Spine:")]
    [SerializeField]
    Vector3 spinePos;
                  
    private void Awake()
    {
        S = this;
    }

    private IEnumerator Start()
    {
        yield return null;
        
        while (magicStick == null)
        {
            magicStick = FindObjectOfType<MagicStick>();
            yield return null;
        }
        magicStick.OnMagicStickTriggerEnter += OnMagicStickTriggerEnter;

        MyKinectManager.S.OnSpinePos += OnSpinePos;
        ActionsManager.OnMagicStickHold += OnMagicStickHold;
        ActionsManager.OnTargetReached += OnTargetReached;
        ActionsManager.OnTargetTimeUp += OnTargetTimeUp;


        while (!MyKinectManager.S.BodyTracked)
            yield return null;
                                                  
        SetTarget(true);
        
        magicStick.SetPosition(spinePos, jointToTrack);
    }

   
    private void OnTargetTimeUp()
    {
        t = 0;
        isTimer = false;

        SetTarget(true);
        SetMagicStick();
    }

    void SetTarget(bool isOn)
    {
        if (isOn)
        {
            newTargetPos = TargetsManager.S.GetTarget();
            jointToTrack = SetRightOrLeft(newTargetPos);
            target.SetTargetAt(spinePos + newTargetPos);
            target.joint = jointToTrack;
        }
        else
        {
            target.ActivateTarget(false);
        }
    }

    void SetMagicStick()
    {
        magicStick.ReleaseStick();
        magicStick.SetPosition(spinePos, jointToTrack);
    }

    private void OnMagicStickHold()
    {
        StartCoroutine(TimerToTarget());
    }

    private void OnTargetReached(bool isTouched)
    {
        StartCoroutine(OnTargetReachedCoroutine());

        
    }

    private IEnumerator OnTargetReachedCoroutine()
    {
        TargetReachedCounter++;

        SetTarget(false);

        t = 0;
        isTimer = false;

        magicStick.SetMagicStickVisible(false);

        yield return StartCoroutine(GameArtManager.S.CreateArtObject(target.transform));

        GameArtManager.S.DestroyArt();

        if (TargetReachedCounter >= NumberOfTargetsNeeded)
            if (ActionsManager.OnGameOver != null)
                ActionsManager.OnGameOver();

        magicStick.SetMagicStickVisible(true);


       // Debug.Log("ArtTimeUP");

        SetTarget(true);
        SetMagicStick();
    }

    private IEnumerator TimerToTarget()
    {

        isTimer = true;
        if (ActionsManager.OnTargetTimerStart != null) ActionsManager.OnTargetTimerStart();

        while (t < AllowedTimeToTarget && isTimer)
        {
            t += Time.deltaTime;
            yield return null;
        }

        if (isTimer)
            if (ActionsManager.OnTargetTimeUp != null) ActionsManager.OnTargetTimeUp();
    }

    public JointToTrack SetRightOrLeft(Vector3 pos)
    {
        if (pos.x > 0) return JointToTrack.RightHand;
        else return JointToTrack.LeftHand;
    } 
  
    private void OnMagicStickTriggerEnter(SetOnPosition gameJointColl)
    {
        if (gameJointColl.jointToTrack == jointToTrack)
        {
            magicStick.HoldMagicStick(gameJointColl.transform);
            if (ActionsManager.OnMagicStickHold != null) ActionsManager.OnMagicStickHold();
        }
    }

    private void OnSpinePos(Vector3 pos)
    {
        spinePos = pos;
    }
}
