﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetOnPosition : MonoBehaviour {
    
	
    [SerializeField]
    public JointToTrack jointToTrack;

	void Start () {

        switch (jointToTrack)
        {
            case JointToTrack.RightHand:
                MyKinectManager.S.OnRightHandPos += OnSetPos;
                break;
            case JointToTrack.LeftHand:
                MyKinectManager.S.OnLeftHandPos += OnSetPos;

                break;
            case JointToTrack.Spine:
                MyKinectManager.S.OnSpinePos += OnSetPos;

                break;
            default:
                break;
        }
        
        	
	}

    [SerializeField]
    float SetZto;

    private void OnSetPos(Vector3 obj)
    {

        
        if (SetZto != -1)
        {
            obj.z = SetZto;
        }
        transform.position = obj;
    }

    
}
