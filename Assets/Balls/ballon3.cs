﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Kinect = Windows.Kinect;

public class ballon3 : MonoBehaviour
{
    public Material whole;
    public Material pop;
    public string activeJoint; // the joint that hit the ball
    public bool isActive = true; // enable activation\disactivation of current ballon

    private int explodetimer = 0;
    private int pintimer = 0;
    private Rigidbody rb;
    private int castflag = 0;
    //private float[] BallTiming = new float[31] { 180, 240, 300, 360, 420, 480, 540, 600, 660, 720, 780, 840, 900, 960, 1020, 1080, 1140, 1200, 1260, 1320, 1380, 1440, 1500, 1560, 1620, 1680, 1740, 1800, 1860, 1920, 9999 }; //9999 was added to prevent exceeding array index after the 30th ball
    private float[] BallTiming = new float[100];
    private float update_count = 0;
    private int i = 0;
    private Color[] colors = new Color[6];
    private int col = 0;

    void Start()
    {
        colors[0] = Color.magenta;
        colors[1] = Color.red;
        colors[2] = Color.green;
        colors[3] = Color.blue;
        colors[4] = Color.yellow;
    }
    void Update()
    {
        if (MyKinectManager.IsTracked == 1)
        {

            if (update_count == 0)

            {
                int BallTiming_length = MyKinectManager.nballs * MyKinectManager.tball + 1;
                BallTiming[0] = 90 + update_count;
                // create the balltiming array
                for (int k = 0; k < BallTiming_length - 1; k++)
                {
                    BallTiming[k] = 90 + update_count + MyKinectManager.isi * 30 * k;
                }
                BallTiming[BallTiming_length - 1] = 9999;
            }

            int[] myorder = MyKinectManager.BallOrder;
            update_count = update_count + 1;

            if (update_count > BallTiming[i])
            {
                if (myorder[i] == 3)
                {
                    if (castflag == 0)
                    {
                        gameObject.tag = "SetBall";
                        castflag = 1;
                    }
                }
                i = i + 1;
            }
        }
        if (CompareTag("SetBall")) // sets ball to fixed xyz distance from player spine on screen
        {
            if (isActive)
            {
                GetComponent<Renderer>().material = whole;

                // Random color shade
                col = Random.Range(0, colors.Length);
                if (col < colors.Length)
                {
                    Renderer renderer = GetComponent<Renderer>();
                    Material mat = renderer.material;
                    float emission = Mathf.PingPong(Time.time, 0.5f);
                    Color baseColor = colors[col];
                    Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
                    mat.SetColor("_EmissionColor", finalColor);
                }

                // determine the position of the ball relative to the skelaton
                Vector3 spineloc = MyKinectManager.ballsoo;
                transform.position = new Vector3(spineloc.x + 4.5F, spineloc.y - 1.5F, spineloc.z - 0.5F);
                rb = GetComponent<Rigidbody>();
                rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
                rb.freezeRotation = true;
            }
            else
            {
                transform.position = new Vector3(30, -30, 30);
            }
            gameObject.tag = "PinnedBall";
        }
        if (CompareTag("HitBall")) // touched ball will increase scall and then transfrom the ball outside of frame
        {
            explodetimer = explodetimer + 1;

            if (explodetimer == 5)
            {
                transform.position = new Vector3(30, -30, 30);
                gameObject.tag = "OutBall";
                castflag = 0;
                explodetimer = 0;
            }
        }
        if (CompareTag("PinnedBall")) // untouched ball for more then 2 sec (60 frames) will transfrom outside of frame
        {
            pintimer = pintimer + 1;
            if (pintimer > 60)
            {
                if (isActive)
                {
                    Vector3 temppos = transform.position;
                    transform.position = temppos + new Vector3(0, 2, 0);
                }
                if (pintimer == 75)
                {
                    transform.position = new Vector3(30, -30, 30);
                    gameObject.tag = "OutBall";
                    castflag = 0;
                    pintimer = 0;
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        activeJoint = collision.collider.gameObject.name;
        if (activeJoint == Kinect.JointType.WristRight.ToString())
        {
            rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
            rb.freezeRotation = true;
            if (CompareTag("PinnedBall"))
            {
                GetComponent<Renderer>().material = pop;
                if (col < colors.Length)
                {
                    Renderer renderer = GetComponent<Renderer>();
                    Material mat = renderer.material;
                    //float emission = Mathf.PingPong(Time.time, 0.5f);
                    Color baseColor = colors[col];
                    Color finalColor = baseColor;// * Mathf.LinearToGammaSpace(emission);
                    mat.SetColor("_EmissionColor", finalColor);
                }
                gameObject.tag = "HitBall";
            }
            pintimer = 0;
        }
    }

}

