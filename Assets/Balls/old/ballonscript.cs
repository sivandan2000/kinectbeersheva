﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ballonscript : MonoBehaviour
{
    private int explodetimer = 0;
    private int pintimer = 0;
    private Rigidbody rb;
    private int castflag = 0;
    //private float[] BallTiming = new float[31] { 180, 240, 300, 360, 420, 480, 540, 600, 660, 720, 780, 840, 900, 960, 1020, 1080, 1140, 1200, 1260, 1320, 1380, 1440, 1500, 1560, 1620, 1680, 1740, 1800, 1860, 1920, 9999 }; //9999 was added to prevent exceeding array index after the 30th ball
    private float[] BallTiming = new float[100];
    private float update_count = 0;
    private int i = 0;
    public Material whole;
    public Material pop;
    private Color[] colors = new Color[6];
    private int col = 0;

    void Start()
    {
        colors[0] = Color.magenta;
        colors[1] = Color.red;
        colors[2] = Color.green;
        colors[3] = Color.blue;
        colors[4] = Color.yellow;
    }
    void Update()
    {
        if (update_count == 0)
        {
            int BallTiming_length = MyKinectManager.nballs * MyKinectManager.tball + 1;
            BallTiming[0] = 180;
            // create the balltiming array
            for (int k = 0; k < BallTiming_length - 1; k++)
            {
                BallTiming[k] = 180 + MyKinectManager.isi * 30 * k;
            }
            BallTiming[BallTiming_length - 1] = 9999;
        }

        int[] myorder = MyKinectManager.BallOrder;
        update_count = update_count + 1;
        if (update_count > BallTiming[i])
        {
            if (myorder[i] > 1)
            {
                if (castflag == 0)
                {
                    gameObject.tag = "SetBall";
                    castflag = 1;
                }
            }
            i = i + 1;
        }
        if (CompareTag("SetBall")) // sets ball to fixed xyz distance from player spine on screen
        {
            GetComponent<Renderer>().material = whole;

            // Random color shade
            col = Random.Range(0, colors.Length );
            if (col < colors.Length)
            {
                Renderer renderer = GetComponent<Renderer>();
                Material mat = renderer.material;
                float emission = Mathf.PingPong(Time.time, 0.5f);
                Color baseColor = colors[col];
                Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
                mat.SetColor("_EmissionColor", finalColor);
            }
       
            //
            Vector3 spineloc = MyKinectManager.ballsoo;
            transform.position = new Vector3(spineloc.x + 7, spineloc.y + 1, spineloc.z);
            rb = GetComponent<Rigidbody>();
            rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
            rb.freezeRotation = true;
            gameObject.tag = "PinnedBall";
        }
        if (CompareTag("HitBall")) // touched ball will increase scall and then transfrom the ball outside of frame
        {
            explodetimer = explodetimer + 1;

            if (explodetimer == 5)
            {
                transform.position = new Vector3(30, 30, 30);
                gameObject.tag = "OutBall";
                castflag = 0;
                explodetimer = 0;
            }
        }
        if (CompareTag("PinnedBall")) // untouched ball for more then 1 sec (30 frames) will transfrom outside of frame
        {
            pintimer = pintimer + 1;
            if (pintimer > 60)
            {
                Vector3 temppos = transform.position;
                transform.position = temppos + new Vector3(0, 2, 0);
                if (pintimer == 75)
                {
                    gameObject.tag = "OutBall";
                    castflag = 0;
                    pintimer = 0;
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezePositionY;
        rb.freezeRotation = true;
        if (CompareTag("PinnedBall"))
        {
            GetComponent<Renderer>().material = pop;
            if (col < colors.Length)
            {
                Renderer renderer = GetComponent<Renderer>();
                Material mat = renderer.material;
                float emission = Mathf.PingPong(Time.time, 0.6f);
                Color baseColor = colors[col];
                Color finalColor = baseColor * Mathf.LinearToGammaSpace(emission);
                mat.SetColor("_EmissionColor", finalColor);
            }
            gameObject.tag = "HitBall";
        }
        pintimer = 0;
    }

}

