﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetZToTransform : MonoBehaviour {

    public Transform TransformToFollow;
    [SerializeField]
    bool isSetZToJoint = true;
                     
    private void Update()
    {
        if (isSetZToJoint)
            if (TransformToFollow != null)  
            {
                Vector3 tPos = transform.position;
                tPos.z = TransformToFollow.position.z;
                transform.position = tPos;
            }
    }
}
