﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTouchTimer : MonoBehaviour {

    public bool isTouched;

	void Start () {

        ActionsManager.OnTargetTouched += OnTargetTouched;
	}

    private void OnTargetTouched(bool isTouched)
    {
        this.isTouched = isTouched;
        if (isTouched) StartCoroutine(TouchTimerCoroutine());
        else StopAllCoroutines();
    }

    private IEnumerator TouchTimerCoroutine()
    {
        float t = 0;

        while (isTouched && t <= MyGameManager.S.NeededTimeToTouchTarget)
        {
            t += Time.deltaTime;
            yield return null;
        }

        if (isTouched)
        {
            if (ActionsManager.OnTargetReached != null) ActionsManager.OnTargetReached(true);
        }
    }
}
