﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    [SerializeField]
    Text targetsCounter;
    
    
	void Start () {

        UpdateTargetsCounter( MyGameManager.S.TargetReachedCounter.ToString());

        ActionsManager.OnTargetReached += OnTargetReached;
	}

    private void OnTargetReached(bool obj)
    {

        StartCoroutine(OnTargetReachedCoroutine());
        

    }

    private IEnumerator OnTargetReachedCoroutine()
    {
        yield return null;
        yield return null;

        UpdateTargetsCounter(MyGameManager.S.TargetReachedCounter.ToString());
    }

    void UpdateTargetsCounter(string str)
    {
        targetsCounter.text = str;
    }
                                                                              
}
