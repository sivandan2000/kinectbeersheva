﻿using UnityEngine;
using System.Collections;
using Kinect = Windows.Kinect;

//Add this Script Directly to ball
public class playsound : MonoBehaviour
{
    public AudioClip score_swap_1_b;    // Add your Audi Clip Here;
                                        // This Will Configure the AudioSource Component; 
                                        // MAke Sure You added AudioSource to the ball;
    private int hasPlayed = 0; 

    void Start()
    {
        GetComponent<AudioSource>().playOnAwake = false;
        GetComponent<AudioSource>().clip = score_swap_1_b;  
    }

    void OnCollisionEnter(Collision collision)  //Plays Sound Whenever collision detected
    {
        string activeJoint = collision.collider.gameObject.name;
        string activeBall = collision.gameObject.name;
        if ((activeJoint == Kinect.JointType.WristRight.ToString() & (activeBall == "1" | activeBall == "3")) | (activeJoint == Kinect.JointType.WristLeft.ToString() & (activeBall == "2" | activeBall == "4")))
        {
            if (hasPlayed == 0)
            {
                GetComponent<AudioSource>().Play();
                hasPlayed = 1;
                StartCoroutine(dontplaytwice());
            }
        }
    }

    IEnumerator dontplaytwice() //Prevent playing sound twice within same collision
    {
        yield return new WaitForSeconds(2);
        hasPlayed = 0;

    }
}

// Make sure that the ball has a collider, box, or mesh.. ect..,
// Make sure to turn "off" collider trigger for your ball Area;
// Make sure That anything that collides into ball, is rigidbody;