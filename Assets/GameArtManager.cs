﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameArtManager : MonoBehaviour {

    public static GameArtManager S;

    public List<GameObject> RewardObjectsPrefabs = new List<GameObject>();
    public List<GameObject> RewardObjects = new List<GameObject>();

    GameObject currentReward;

    public float TimeBetweenRewardAndNextObject = 3f;

    private void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    private void Start()
    {
        CreateArtRewards();

    }

    private void CreateArtRewards()
    {
        foreach (var obj in RewardObjectsPrefabs)
        {
           // Debug.Log("WTF");
            GameObject RewardGO = Instantiate(obj, transform) as GameObject;
            RewardObjects.Add(RewardGO);
            RewardGO.SetActive(false);
       //     RewardGO.SetActive(false);

        }
    }

    public IEnumerator CreateArtObject(Transform parent, int index = -1)
    {
        yield return null;

        if (index == -1 || index >= RewardObjects.Count) index = UnityEngine.Random.Range(0, RewardObjects.Count);

        currentReward = RewardObjects[index];
        currentReward.SetActive(true);
        currentReward.transform.position = parent.position;

        yield return new WaitForSeconds(TimeBetweenRewardAndNextObject);

    }

    public void DestroyArt()
    {
        currentReward.SetActive(false);
        currentReward = null;
    }
}
