﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using System.Diagnostics;
using System;
using System.IO;
using System.Text;
using UnityEngine.UI;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Linq;


public class FileManager : MonoBehaviour {

    public Action OnSaveData;
    public Action<string> OnAddToMovementLog;


    [SerializeField]
    private string fileDir;// = "C:/Users/tamar/OneDrive/Documents/BodyData/";
    [SerializeField]
    private string movementFileName = "";// DateTime.Now.ToString("yyyy-MM-ddThhmmss"); // generate a name for the outpot file
    [SerializeField]
    private string actionsFileName = "";// DateTime.Now.ToString("yyyy-MM-ddThhmmss"); // generate a name for the outpot file


    private string fileDate = DateTime.Now.ToString("yyyy-MM-ddThhmmss"); // generate the date for the outpot file
    
    private List<String> MovementLog = new List<string>();
    public List<String> ActionsLog = new List<string>();

    
    public StringBuilder str = new StringBuilder();
    private string[] movementDataStr;
    private string[] actionsDataStr;
    public static FileManager S;

    void Awake()
    {
        S = this;
    }

    IEnumerator Start()
    {
        yield return null;

        if (PlayerInfo.SavingPath != "") fileDir = PlayerInfo.SavingPath;

        string CurrentDate = DateTime.Now.Year.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString();
        movementFileName = PlayerInfo.ID.ToString() + "_" +  CurrentDate +"_Movement";
        actionsFileName = PlayerInfo.ID.ToString() + "_" +  CurrentDate +"_Actions";

        MovementLog.Add("Date: " + DateTime.Now + " ID: " + PlayerInfo.ID.ToString() + " Age: " + PlayerInfo.Age.ToString() + " Sex: " + PlayerInfo.Sex.ToString());
        ActionsLog.Add("Date: " + DateTime.Now + " ID: " + PlayerInfo.ID.ToString() + " Age: " + PlayerInfo.Age.ToString() + " Sex: " + PlayerInfo.Sex.ToString());


        InputManager.S.OnEscPressed += OnEscPressed;
        ActionsManager.OnGameOver += OnEscPressed;



        OnSaveData += SaveData;
        OnAddToMovementLog += AddToMovementLog;

        ActionsManager.OnMagicStickHold += () => AddToActionsLog("OnMagicStickHold " + MyGameManager.S.magicStick.transform.position);
        ActionsManager.OnTargetReached += (x) => AddToActionsLog("OnTargetReached " + MyGameManager.S.target.transform.position);
        ActionsManager.OnTargetTimeUp += () => AddToActionsLog("OnTargetTimerUp");

        ActionsManager.OnGetTarget += (index) => AddToActionsLog("OnGetTarget " + index + 1 + " " + TargetsManager.S.targetPositions[index].ToString());

    }

    private void AddToMovementLog(string s)
    {
        MovementLog.Add(s.ToString());
    }

    private void AddToActionsLog(string s)
    {
        ActionsLog.Add("Frame: " + MyKinectManager.frameCounter + ", " + Time.time + "," + s.ToString());
    }


    void SaveData()
    {
        StartCoroutine(SaveDataCoroutine());
    }

    private void OnEscPressed()
    {
        if (OnSaveData != null) OnSaveData();
    }

    IEnumerator SaveDataCoroutine()
    {                                         
        movementDataStr = MovementLog.ToArray();
        actionsDataStr = ActionsLog.ToArray();

        File.WriteAllLines(fileDir + movementFileName + ".txt", movementDataStr, Encoding.UTF8);
        File.WriteAllLines(fileDir + actionsFileName + ".txt", actionsDataStr, Encoding.UTF8);


        yield return new WaitForFixedUpdate();
        Application.Quit();
    }
}
