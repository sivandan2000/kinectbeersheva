﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class TargetsManager : MonoBehaviour {

    public static TargetsManager S;

    public List<Vector3> targetPositions;

    List<Vector3> CurrentTargets = new List<Vector3>();

    private void Awake()
    {
        S = this;
    }

    private void Start()
    {
        CurrentTargets = CreateNewTargets();
        
    }

    public Vector3 GetTarget()
    {   
        Vector3 target = CurrentTargets[CurrentTargets.Count - 1];
        CurrentTargets.RemoveAt(CurrentTargets.Count - 1);

        if (CurrentTargets.Count == 0)
        {
            CurrentTargets = CreateNewTargets();
        }


        if (ActionsManager.OnGetTarget != null) ActionsManager.OnGetTarget(targetPositions.IndexOf(target));  //index);

        //        if (ActionsManager.OnGetTarget != null) ActionsManager.OnGetTarget();
        Debug.Log("GetTarget: " + target);
        return target;

    }

    public List<Vector3> CreateNewTargets()
    {
        List<Vector3> newList = Randomize(new List<Vector3>(targetPositions));
        return newList; 

    }
                 

    public static List<T> Randomize<T>(List<T> list)
    {
        List<T> randomizedList = new List<T>();
        System.Random rnd = new System.Random();
        while (list.Count > 0)
        {
            int index = rnd.Next(0, list.Count); //pick a random item from the master list
            randomizedList.Add(list[index]); //place it at the end of the randomized list
           // Debug.Log(list[index]);
            list.RemoveAt(index);
        }
        return randomizedList;
    }
  

	
}
