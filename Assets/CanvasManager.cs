﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour {

    public static CanvasManager S;

    public InputField ID;
    public InputField Age;
    public InputField Sex;
    public InputField SavingPath;

    public Toggle DrawSkeleton;

    private void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    public void OnIDSet(string str)
    {
        PlayerInfo.ID = int.Parse(ID.text);
        Debug.Log(PlayerInfo.ID);
    }

    public void OnAgeSet(string str)
    {
        PlayerInfo.Age = int.Parse(Age.text);
        Debug.Log(PlayerInfo.Age);
    }

    public void OnSexSet(string str)
    {
        PlayerInfo.Sex = Sex.text;
        Debug.Log(PlayerInfo.Sex);
    }

    public void OnSavingPathSet(string str)
    {
        PlayerInfo.SavingPath = SavingPath.text;
        Debug.Log(PlayerInfo.SavingPath);
    }


    public void OnDrawSkeletonChange()
    {
        PlayerInfo.isDrawSkeleton = DrawSkeleton.isOn;
        Debug.Log(PlayerInfo.isDrawSkeleton);
    }


    public void OnPlayButtonPressed()
    {
        SceneManager.LoadScene(1);
    }


}
