﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager S;

    public AudioSource Win1;
    public AudioSource Win2;

    public AudioSource MagicStick;

    public AudioSource Lose;

    public AudioSource Timer;
    public AudioSource Animals;

    public AudioSource NextTarget;

    public AudioSource WaitForMagicStick;


    private void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    private void Start()
    {
        ActionsManager.OnTargetReached += OnTargetReached;
        ActionsManager.OnMagicStickHold += OnMagicStickHold;
        ActionsManager.OnTargetTimeUp += OnTargetTimeUp;
        ActionsManager.OnTargetTimerStart += OnTargetTimerStart;

        ActionsManager.OnGetTarget += OnGetTarget;

    }

    private void OnGetTarget(int index)
    {
        NextTarget.Play();
        WaitForMagicStick.Play();
    }

    private void OnTargetTimerStart()
    {
        Debug.Log("TimerStart");
        Timer.Play();
    }

    private void OnTargetTimeUp()
    {
        Lose.Play();
        Timer.Stop();
    }

    private void OnMagicStickHold()
    {
        MagicStick.Play();
        WaitForMagicStick.Stop();

    }

    private void OnTargetReached(bool isReached)
    {

        Timer.Stop();

        if (isReached)
        {
            Win1.Play();
            Win2.Play();
        }
        else Lose.Play();

    }
}
