﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class pressplay : MonoBehaviour {

    public Button myButton;
    private string id = "";
    private string age = "";
    private string sex = "";
    public static string filename = "";
    public static int drawSkel = 1;
    private bool isShowing;
    public GameObject canv;
    public GameObject pla;
    GameObject inGameToggle;

    private void Start()
    {
       inGameToggle = GameObject.Find("draw");
    }

    public void OnClick()

    {
        id = id_input.txtinput;
        age = age_input.txtinput;
        sex = sex_input.txtinput;
        
        try
        {
            int key = Int32.Parse(id) * 3 - 2015;
            filename = key + "_" + age + "_" + sex;
        }
        catch (FormatException e)
        {
            Debug.Log(e.Message);
            filename = id + "_" + age + "_" + sex;
        }

        if (inGameToggle.GetComponent<Toggle>().isOn)
        {
            drawSkel = 1;
        }

        Debug.Log(filename);
        isShowing = !isShowing;
        canv.SetActive(isShowing);
        pla.SetActive(isShowing);
        //SceneManager.LoadScene("colorcam_scene", LoadSceneMode.Single);
        SceneManager.LoadScene("colorcam_sceneDan", LoadSceneMode.Single);

    }

}
