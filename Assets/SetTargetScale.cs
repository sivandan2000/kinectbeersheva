﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTargetScale : MonoBehaviour {

    [SerializeField]
    Animator anim;


    Vector3 birthScale;

    [SerializeField]
    float smallScale = 0.3f;

    [SerializeField]
    float bigScale = 1f ;


    

    private void Awake()
    {
        birthScale = transform.localScale;

        if (anim == null) anim = GetComponent<Animator>();
        if (anim == null) anim = GetComponentInChildren<Animator>();
    }

    void Start () {

        ActionsManager.OnMagicStickHold += OnMagicStickHold;
        ActionsManager.OnGetTarget += OnGetTarget;
        ActionsManager.OnTargetTouched += OnTargetTouched;	
	}

    private void OnTargetTouched(bool isTouched)
    {

        if (isTouched)
        {
            anim.SetBool("IsScale", false);
        }
        else
        {
            anim.SetBool("IsScale", true);

        }
    }

    private void OnGetTarget(int index)
    {
        anim.SetBool("IsScale", false);

        ChangeScale(smallScale);
    
    }

    private void OnMagicStickHold()
    {
        ChangeScale(bigScale);

        anim.SetBool("IsScale", true);
    }

    void ChangeScale(float ScaleFactor)
    {
        transform.localScale = birthScale * ScaleFactor;
    }

    
}
